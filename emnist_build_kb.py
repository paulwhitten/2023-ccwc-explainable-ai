import argparse
from load_emnist_resnet_data import load_emnist_resnet, load_mnist_labels
from plot_history import plot_model_history
from multiprocessing import Process
from datetime import datetime
import numpy as np
from tensorflow import keras
import tensorflow as tf
import json

"""
This program reprocesses all of the transform data to build a knowledgbase. In
doing so, we read all transforms and load models, outputting all predictions for
the transforms based on training and then test images. Data is output in json
format to files for each of the datasets along with json contining the labels.
"""

def eval_images(batch):
    print("====", batch[0], "====")
    batch_start_time = datetime.now()
    # load the image data to process
    train_images, train_labels, num_train_labels = load_emnist_resnet(batch[1], batch[2])
    test_images, test_labels, num_test_labels = load_emnist_resnet(batch[3], batch[4])
    train_label_list = load_mnist_labels(batch[2])
    test_label_list = load_mnist_labels(batch[4])
    train_label_max = np.max(train_label_list)
    train_label_min = np.min(train_label_list)
    print("label min:", train_label_min, "label max:", train_label_max)
    # create the model

    resnet_model = keras.models.load_model(batch[6] + "/" + batch[0] + ".model")

    train_predictions = resnet_model.predict(train_images)
    test_predictions = resnet_model.predict(test_images)

    print("train predictions shape:", train_predictions.shape)
    print("test predictions shape:", test_predictions.shape)

    with open(batch[5] + "/" + batch[0] + ".train.json", 'w') as outfile:
        json.dump(train_predictions.tolist(), outfile)
    with open(batch[5] + "/" + batch[0] + ".test.json", 'w') as outfile:
        json.dump(test_predictions.tolist(), outfile)
    with open(batch[5] + "/" + batch[0] + ".train-labels.json", 'w') as outfile:
        json.dump(train_label_list, outfile)
    with open(batch[5] + "/" + batch[0] + ".test-labels.json", 'w') as outfile:
        json.dump(test_label_list, outfile)

    batch_end_time = datetime.now()
    print(batch[0], "done in:", batch_end_time - batch_start_time)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='trains transforms for a set')
    parser.add_argument('-r', '--train_folder', 
                        help='The training input folder')
    parser.add_argument('-e', '--test_folder', 
                        help='The test input folder')
    parser.add_argument('-o', '--output_folder', 
                        help='The folder to output')
    parser.add_argument('-m', '--model_folder', 
                        help='The folder containing the models')
    args = parser.parse_args()

    batches = [
        ["raw", f'{args.train_folder}/raw-image', f'{args.train_folder}/raw-labels',
        f'{args.test_folder}/raw-image', f'{args.test_folder}/raw-labels', args.output_folder, args.model_folder],

        ["crossing", f'{args.train_folder}/crossing-image', f'{args.train_folder}/crossing-labels',
        f'{args.test_folder}/crossing-image', f'{args.test_folder}/crossing-labels', args.output_folder, args.model_folder],

        ["endpoint", f'{args.train_folder}/endpoint-image', f'{args.train_folder}/endpoint-labels',
        f'{args.test_folder}/endpoint-image', f'{args.test_folder}/endpoint-labels', args.output_folder, args.model_folder],

        ["fill", f'{args.train_folder}/fill-image', f'{args.train_folder}/fill-labels',
        f'{args.test_folder}/fill-image', f'{args.test_folder}/fill-labels', args.output_folder, args.model_folder],

        ["skel-fill", f'{args.train_folder}/skel-fill-image', f'{args.train_folder}/skel-fill-labels',
        f'{args.test_folder}/skel-fill-image', f'{args.test_folder}/skel-fill-labels', args.output_folder, args.model_folder],

        ["skel", f'{args.train_folder}/skel-image', f'{args.train_folder}/skel-labels',
        f'{args.test_folder}/skel-image', f'{args.test_folder}/skel-labels', args.output_folder, args.model_folder],

        ["thresh", f'{args.train_folder}/thresh-image', f'{args.train_folder}/thresh-labels',
        f'{args.test_folder}/thresh-image', f'{args.test_folder}/thresh-labels', args.output_folder, args.model_folder],

        ["line", f'{args.train_folder}/line-image', f'{args.train_folder}/line-labels',
        f'{args.test_folder}/line-image', f'{args.test_folder}/line-labels', args.output_folder, args.model_folder],

        ["ellipse", f'{args.train_folder}/ellipse-image', f'{args.train_folder}/ellipse-labels',
        f'{args.test_folder}/ellipse-image', f'{args.test_folder}/ellipse-labels', args.output_folder, args.model_folder],

        ["circle", f'{args.train_folder}/circle-image', f'{args.train_folder}/circle-labels',
        f'{args.test_folder}/circle-image', f'{args.test_folder}/circle-labels', args.output_folder, args.model_folder],

        ["ellipse-circle", f'{args.train_folder}/ellipse_circle-image', f'{args.train_folder}/ellipse_circle-labels',
        f'{args.test_folder}/ellipse_circle-image', f'{args.test_folder}/ellipse_circle-labels', args.output_folder, args.model_folder],

        ["chull", f'{args.train_folder}/chull-image', f'{args.train_folder}/chull-labels',
        f'{args.test_folder}/chull-image', f'{args.test_folder}/chull-labels', args.output_folder, args.model_folder]
    ]

    start_time = datetime.now()

    for batch in batches:
        p = Process(target=eval_images, args=(batch,))
        p.start()
        p.join()

    end_time = datetime.now()
    print("Program done in:", end_time - start_time)
        
