import matplotlib.pyplot as plt

def plot_model_history(history, metric_name, plot_name, folder):
    history.history[metric_name].insert(0, 0.0)
    fig, axs = plt.subplots(1)
    # summarize history for accuracy
    axs.plot(history.history[metric_name])
    #axs[0].plot(history.history['val_' + metric_name]) 
    axs.set_title(plot_name + ' model ' + metric_name)
    axs.set_ylabel('Accuracy')
    axs.set_xlabel('Epoch')
    
    axs.legend(['train', 'validate'], loc='upper left')
    # summarize history for loss
    #axs[1].plot(history.history['loss'])
    #axs[1].plot(history.history['val_loss']) 
    #axs[1].set_title('Model Loss')
    #axs[1].set_ylabel('Loss')
    #axs[1].set_xlabel('Epoch')
    #axs[1].legend(['train', 'validate'], loc='upper left')
    #plt.ylim([0.0, 1.00])
    axs.set_ylim([0.0, 1.0])
    axs.set_xlim(xmin=0, xmax=len(history.history[metric_name])-1)
    fig.savefig(folder + "/" + 'history_plot-' + plot_name + '.png')