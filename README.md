# 2023-ccwc-explainable-ai

This repo contains the implementation from [Explainable Neural Network Recognition of Handwritten Characters](https://doi.org/10.1109/CCWC57344.2023.10099288).
Source for that paper is [here](https://github.com/paulwhitten/2023-ccwc-Explainable_Neural_Network_Recognition_of_Handwritten_Characters).

## method

1. transform images using `transform_parallel.py`
2. train property transforms by running `emnist_train_transforms.py` to attain resnet NNA 50 models
3. run data through models with `emnist_build_kb.py`, stores stats for each model and training set
   in a file, including training and test.  Output will be 4 json files for each transform. 
4. run `emnist_build_all_with_names_json.py` to process the transform data to get results.
   This will:
   1. build the knowledgebase for the training data and get property predictions
      the probability each transform has at predicting each class
   2. process the test data to see how it is classified
   3. output stats on all

